package com.birkle.vehiclesystem.utils;

/**
 * Constant values used in this application
 *
 * @author VinodJohn
 */
public class Constants {
    public static class WebService {
        public static final String PORT_NAME = "WebServicesPort";
        public static final String LOCATION_URI = "/ws";
        public static final String NAMESPACE_URI = "http://www.springframework.org/schema/web-services";
    }

    public static class Security {
        public static final String AUTHORITY_ADMIN = "ROLE_ADMIN";
        public static final String AUTHORITY_REGULAR_USER = "ROLE_REGULAR_USER";
        public static final String AUTHORITY_SUPER_USER = "ROLE_SUPER_USER";
        public static final String DEFAULT_USER_ADMIN_EMAIL = "admin@vms.com";
        public static final String DEFAULT_USER_ADMIN_PASSWORD = "vmsadmin";
    }

    public static class Audit {
        public static final String DEFAULT_AUDITOR = "SYSTEM";
    }

    public static class Client {
        public static final String BASE_URL = "http://localhost:4200";
        public static final String USER_ACTIVATION_URL = "/auth/activate-user";
        public static final String RESET_PASSWORD_URL = "/auth/reset-password";
        public static final String SESSION_EXPIRED_URL = "/auth/session-expired";
        public static final int USER_ACTIVATION_URL_LIFE_HOURS = 5;
        public static final int RESET_PASSWORD_LIFE_MINUTES = 10;
    }
}
