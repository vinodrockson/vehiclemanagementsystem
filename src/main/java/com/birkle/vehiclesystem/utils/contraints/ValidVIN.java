package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for VIN validation
 *
 * @author VinodJohn
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {VINValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidVIN {
    String message() default "{messages.constraints.invalid-vin}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
