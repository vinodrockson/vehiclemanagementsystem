package com.birkle.vehiclesystem.utils.contraints;

import com.birkle.vehiclesystem.models.Authority;
import com.birkle.vehiclesystem.services.security.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check if authority already exists
 *
 * @author VinodJohn
 */
public class UniqueAuthorityValidator implements ConstraintValidator<UniqueAuthority, Authority> {
    @Autowired
    private AuthorityService authorityService;

    @Override
    public void initialize(UniqueAuthority constraintAnnotation) {
    }

    @Override
    public boolean isValid(Authority authority, ConstraintValidatorContext constraintValidatorContext) {
        return authority.getId() == null && authorityService.getAuthorityByName(authority.getName()) != null;
    }
}
