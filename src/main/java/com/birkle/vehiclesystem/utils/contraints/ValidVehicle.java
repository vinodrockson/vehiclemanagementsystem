package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for country validation
 *
 * @author VinodJohn
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {VehicleValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidVehicle {
    String message() default "{messages.constraints.invalid-vehicle}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
