package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check VIN format
 *
 * @author VinodJohn
 */
public class VINValidator implements ConstraintValidator<ValidVIN, String> {
    @Override
    public void initialize(ValidVIN constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        try {
            int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 0, 7, 0, 9,
                    2, 3, 4, 5, 6, 7, 8, 9}, weights = {8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2};
            int sum = 0;
            String tempStr = s;

            tempStr = tempStr.replaceAll("-", "");
            tempStr = tempStr.replaceAll(" ", "");
            tempStr = tempStr.toUpperCase();

            if (tempStr.length() != 17) {
                throw new RuntimeException("Invalid VIN:" + s + ". VIN number must be 17 characters");
            }

            for (int i = 0; i < 17; i++) {
                char c = tempStr.charAt(i);
                int value;
                int weight = weights[i];

                if (c >= 'A' && c <= 'Z') {
                    value = values[c - 'A'];
                    if (value == 0)
                        throw new RuntimeException("Invalid VIN :" + s + ". Illegal character: " + c);
                } else if (c >= '0' && c <= '9') {
                    value = c - '0';
                } else {
                    throw new RuntimeException("Invalid VIN :" + s + ". Illegal character: " + c);
                }

                sum = sum + weight * value;
            }

            sum = sum % 11;
            char check = tempStr.charAt(8);
            return (sum == 10 && check == 'X') || (sum == transliterate(check));
        } catch (RuntimeException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }
        }
        return false;
    }

    // PRIVATE METHODS //
    private int transliterate(char check) {
        if (check == 'A' || check == 'J') {
            return 1;
        } else if (check == 'B' || check == 'K' || check == 'S') {
            return 2;
        } else if (check == 'C' || check == 'L' || check == 'T') {
            return 3;
        } else if (check == 'D' || check == 'M' || check == 'U') {
            return 4;
        } else if (check == 'E' || check == 'N' || check == 'V') {
            return 5;
        } else if (check == 'F' || check == 'W') {
            return 6;
        } else if (check == 'G' || check == 'P' || check == 'X') {
            return 7;
        } else if (check == 'H' || check == 'Y') {
            return 8;
        } else if (check == 'R' || check == 'Z') {
            return 9;
        } else {
            return Character.getNumericValue(check);
        }
    }
}
