package com.birkle.vehiclesystem.utils.contraints;

import com.birkle.vehiclesystem.models.User;
import com.birkle.vehiclesystem.services.security.AuthorityService;
import com.birkle.vehiclesystem.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check user's email already exists and valid Authority
 *
 * @author VinodJohn
 */
public class UniqueUserValidator implements ConstraintValidator<UniqueUser, User> {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthorityService authorityService;

    @Override
    public void initialize(UniqueUser constraintAnnotation) {
    }

    @Override
    public boolean isValid(User user, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if (user.getId() == null && userService.getUserByEmail(user.getEmail()) != null) {
                return false;
            } else if (user.getAuthority() != null && authorityService.getAuthorityByName(user.getAuthority().getName()) == null) {
                throw new RuntimeException("{messages.constraints.unique-user-authority}");
            } else {
                return true;
            }
        } catch (RuntimeException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }

        }
        return false;
    }
}
