package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for Enum validation
 *
 * @author VinodJohn
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {EnumValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEnum {
    String message() default "{messages.constraints.invalid-value}";

    Class<? extends java.lang.Enum<?>> enumClass();

    boolean ignoreCase() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
