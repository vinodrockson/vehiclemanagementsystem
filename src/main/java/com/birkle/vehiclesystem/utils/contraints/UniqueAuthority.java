package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for unique email validation
 *
 * @author VinodJohn
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {UniqueAuthorityValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueAuthority {
    String message() default "{messages.constraints.unique-authority}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
