package com.birkle.vehiclesystem.utils.contraints;

import com.birkle.vehiclesystem.models.Vehicle;
import com.birkle.vehiclesystem.services.vehicle.VehicleService;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check vehicle's VIN and country
 *
 * @author VinodJohn
 */
public class VehicleValidator implements ConstraintValidator<ValidVehicle, Vehicle> {
    @Autowired
    private VehicleService vehicleService;

    @Override
    public void initialize(ValidVehicle constraintAnnotation) {
    }

    @Override
    public boolean isValid(Vehicle vehicle, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if (vehicle.getId() == null && vehicleService.getVehicleByVIN(vehicle.getVin()) != null) {
                throw new RuntimeException("Invalid VIN: " + vehicle.getVin() + ". VIN already exists!");
            } else if (vehicle.getManufacturingCountry().isBlank() || CountryCode.getByAlpha3Code(vehicle.getManufacturingCountry()) == null ||
                    vehicle.getPlateCountry().isBlank() || CountryCode.getByAlpha3Code(vehicle.getPlateCountry()) == null) {
                throw new RuntimeException("{messages.constraints.invalid-country}");
            } else {
                return true;
            }
        } catch (RuntimeException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }

        }

        return false;
    }
}
