package com.birkle.vehiclesystem.utils.contraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check Enum
 *
 * @author VinodJohn
 */
public class EnumValidator implements ConstraintValidator<ValidEnum, String> {
    private ValidEnum enumAnnotation;

    @Override
    public void initialize(ValidEnum constraintAnnotation) {
        this.enumAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        Object[] enumValues = this.enumAnnotation.enumClass().getEnumConstants();

        if (enumValues != null) {
            for (Object enumValue : enumValues) {
                if (s.equals(enumValue.toString())
                        || (this.enumAnnotation.ignoreCase() && s.equalsIgnoreCase(enumValue.toString()))) {
                    return true;
                }
            }
        }

        return false;
    }
}
