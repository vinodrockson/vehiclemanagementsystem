package com.birkle.vehiclesystem.handlers.exception.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Model for Error response
 *
 * @author VinodJohn
 */
@Data
@AllArgsConstructor
public class ErrorResponse {
    
    private String message;
    private List<String> details;
}
