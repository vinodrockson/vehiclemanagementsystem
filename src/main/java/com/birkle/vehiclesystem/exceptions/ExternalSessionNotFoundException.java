package com.birkle.vehiclesystem.exceptions;

import java.util.UUID;

/**
 * Exception to handle ExternalSession's unavailability
 *
 * @author VinodJohn
 */
public class ExternalSessionNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public ExternalSessionNotFoundException() {
        super("Session not found!");
    }
}
