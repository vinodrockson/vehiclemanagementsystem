package com.birkle.vehiclesystem.exceptions;

import java.util.UUID;

/**
 * Exception to handle User's unavailability
 *
 * @author VinodJohn
 */
public class UserNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public UserNotFoundException(UUID id) {
        super("User not found! (ID: " + id.toString() + ")");
    }
}
