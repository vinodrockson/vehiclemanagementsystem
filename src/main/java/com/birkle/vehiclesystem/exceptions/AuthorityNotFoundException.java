package com.birkle.vehiclesystem.exceptions;

import java.util.UUID;

/**
 * Exception to handle Authority's unavailability
 *
 * @author VinodJohn
 */
public class AuthorityNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public AuthorityNotFoundException(UUID id) {
        super("Authority not found! (ID: " + id.toString() + ")");
    }
}

