package com.birkle.vehiclesystem.exceptions;

import java.util.UUID;

/**
 * Exception to handle Vehicle's unavailability
 *
 * @author VinodJohn
 */
public class VehicleNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public VehicleNotFoundException(UUID id) {
        super("Vehicle not found! (ID: " + id.toString() + ")");
    }
}
