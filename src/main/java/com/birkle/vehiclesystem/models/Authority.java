package com.birkle.vehiclesystem.models;

import com.birkle.vehiclesystem.utils.contraints.UniqueAuthority;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

/**
 * Authority model
 *
 * @author VinodJohn
 */

@Entity
@Data
@UniqueAuthority
@EqualsAndHashCode(callSuper = true)
public class Authority extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotBlank(message = "{messages.constraints.blank-authority}")
    private String name;

    @JsonIgnore
    private boolean isActive;
}
