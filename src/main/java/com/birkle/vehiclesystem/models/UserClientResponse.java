package com.birkle.vehiclesystem.models;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Model of user to send as response client
 *
 * @author VinodJohn
 */
@Data
@AllArgsConstructor
public class UserClientResponse {
    private final String email;
    private final boolean isEmailActivated;
    private final boolean isActive;
    private final String authority;
}
