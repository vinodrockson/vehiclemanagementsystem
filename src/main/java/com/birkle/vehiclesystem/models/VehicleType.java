package com.birkle.vehiclesystem.models;

/**
 * Vehicle types
 *
 * @author VinodJohn
 */
public enum VehicleType {
    CAR, SUV, BUS, MOTORBIKE, ATV, COMMERCIAL_VEHICLE
}
