package com.birkle.vehiclesystem.models;

public enum ExternalSessionType {
    CREATE_USER, RESET_USER_PASSWORD
}
