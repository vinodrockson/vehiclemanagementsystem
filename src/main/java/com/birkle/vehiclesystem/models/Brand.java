package com.birkle.vehiclesystem.models;

/**
 * Brands
 *
 * @author VinodJohn
 */
public enum Brand {
    BMW, AUDI, MAYBACH, LAMBORGHINI, BUGATTI, FERRARI, MERCEDES, TOYOTA, TESLA, PEUGEOT, LEXUS, LINCOLN, ROLLS_ROYCE,
    SUBARU, DUCATI, HONDA, ACURA, CHEVY, FORD, DODGE, HUMMER, VOLSWAGEN, VOLVO

}
