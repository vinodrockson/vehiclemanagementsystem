package com.birkle.vehiclesystem.models;

import com.birkle.vehiclesystem.utils.contraints.ValidVehicle;
import com.birkle.vehiclesystem.utils.contraints.ValidEnum;
import com.birkle.vehiclesystem.utils.contraints.ValidVIN;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Vehicle model
 *
 * @author VinodJohn
 */
@Entity
@Data
@ValidVehicle
@EqualsAndHashCode(callSuper = true)
public class Vehicle extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @ValidVIN
    @Column(unique = true)
    private String vin;

    @ValidEnum(enumClass = Brand.class, message = "{messages.constraints.invalid-brand}")
    private String brand;

    @NotBlank(message = "{messages.constraints.blank-model}")
    private String model;

    @ValidEnum(enumClass = VehicleType.class, message = "{messages.constraints.invalid-vehicle}")
    private String vehicleType;

    private String plateCountry;

    @NotBlank(message = "{messages.constraints.blank-plate-number}")
    private String plateNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past(message = "{messages.constraints.past-manufacturing-date}")
    @NotNull(message = "{messages.constraints.null-manufacturing-date}")
    private LocalDate manufacturingDate;

    private String manufacturingCountry;

    @JsonIgnore
    private boolean isActive;
}
