package com.birkle.vehiclesystem.configurations;

import com.birkle.vehiclesystem.services.userdetails.CustomUserDetailsService;
import com.birkle.vehiclesystem.utils.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

/**
 * Configuration for security
 *
 * @author VinodJohn
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new CustomUserDetailsService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
        configuration.setExposedHeaders(Collections.singletonList("x-auth-token"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String role = "ROLE_",
                admin = Constants.Security.AUTHORITY_ADMIN.replace(role, ""),
                regularUser = Constants.Security.AUTHORITY_REGULAR_USER.replace(role, ""),
                superUser = Constants.Security.AUTHORITY_SUPER_USER.replace(role, "");

        http.authorizeRequests()
                .antMatchers("/", "/user/create", "/user/activate/*", "/user/reset-password-generate/*",
                        "/user/reset-password/*")
                .permitAll()
                .antMatchers("/authority/*", "/user", "/user/delete/*", "/user/restore/*").hasRole(admin)
                .antMatchers("/auth/login", "/authority", "/user/*", "/user/update/*", "/vehicle", "/vehicle/*")
                .hasAnyRole(admin, superUser, regularUser)
                .antMatchers("/vehicle/create", "/vehicle/update/*", "/vehicle/delete/*", "/vehicle/restore/*")
                .hasAnyRole(superUser, admin)
                .and()
                .httpBasic()
                .and()
                .logout().permitAll(false)
                .and()
                .sessionManagement().invalidSessionUrl("/auth/session-invalid")
                .and()
                .cors()
                .and()
                .csrf().disable();
    }
}
