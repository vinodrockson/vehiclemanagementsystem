package com.birkle.vehiclesystem.soapservices;

import org.apache.commons.text.WordUtils;
import org.springframework.schema.web_services.GetAllVehicleTypesRequest;
import org.springframework.schema.web_services.GetAllVehicleTypesResponse;
import org.springframework.schema.web_services.VehicleType;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.birkle.vehiclesystem.utils.Constants.WebService.NAMESPACE_URI;

/**
 * Web service to handle vehicle type requests
 *
 * @author VinodJohn
 */
@Endpoint
public class VehicleTypeEndPoint {
    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllVehicleTypesRequest")
    public GetAllVehicleTypesResponse getAllVehicleType(@RequestPayload GetAllVehicleTypesRequest request) {
        List<VehicleType> vehicleTypes = new ArrayList<>();

        Arrays.asList(com.birkle.vehiclesystem.models.VehicleType.values()).forEach(brand -> {
            VehicleType vehicleBrand = new VehicleType();
            vehicleBrand.setId(brand.name());
            vehicleBrand.setValue(WordUtils.capitalizeFully(brand.name(), '_').replaceAll("_", " "));
            vehicleTypes.add(vehicleBrand);
        });

        GetAllVehicleTypesResponse response = new GetAllVehicleTypesResponse();
        response.setVehicleType(vehicleTypes);
        return response;
    }
}
