package com.birkle.vehiclesystem.soapservices;

import com.birkle.vehiclesystem.configurations.WebServiceConfiguration;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.schema.web_services.Country;
import org.springframework.schema.web_services.GetAllCountriesRequest;
import org.springframework.schema.web_services.GetAllCountriesResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.birkle.vehiclesystem.utils.Constants.WebService.NAMESPACE_URI;

/**
 * Web service to handle country requests
 *
 * @author VinodJohn
 */
@Endpoint
public class CountryEndPoint {
    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllCountriesRequest")
    public GetAllCountriesResponse getAllCountries(@RequestPayload GetAllCountriesRequest request) {
        List<Country> countries = new ArrayList<>();

        Arrays.asList(CountryCode.values()).forEach(countryCode -> {
            Country country = new Country();
            country.setCode(countryCode.getAlpha3());
            country.setName(countryCode.getName());
            countries.add(country);
        });

        GetAllCountriesResponse response = new GetAllCountriesResponse();
        response.setCountry(countries);
        return response;
    }
}

