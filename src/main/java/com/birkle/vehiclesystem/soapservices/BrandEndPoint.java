package com.birkle.vehiclesystem.soapservices;

import com.birkle.vehiclesystem.configurations.WebServiceConfiguration;
import com.birkle.vehiclesystem.models.Brand;
import org.apache.commons.text.WordUtils;
import org.springframework.schema.web_services.GetAllBrandsRequest;
import org.springframework.schema.web_services.GetAllBrandsResponse;
import org.springframework.schema.web_services.VehicleBrand;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.birkle.vehiclesystem.utils.Constants.WebService.NAMESPACE_URI;

/**
 * Web service to handle brand requests
 *
 * @author VinodJohn
 */
@Endpoint
public class BrandEndPoint {
    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBrandsRequest")
    public GetAllBrandsResponse getAllBrands(@RequestPayload GetAllBrandsRequest request) {
        List<VehicleBrand> brands = new ArrayList<>();

        Arrays.asList(Brand.values()).forEach(brand -> {
            VehicleBrand vehicleBrand = new VehicleBrand();
            vehicleBrand.setId(brand.name());
            vehicleBrand.setName(WordUtils.capitalizeFully(brand.name(), '_').replaceAll("_", " "));
            brands.add(vehicleBrand);
        });

        GetAllBrandsResponse response = new GetAllBrandsResponse();
        response.setBrand(brands);
        return response;
    }
}
