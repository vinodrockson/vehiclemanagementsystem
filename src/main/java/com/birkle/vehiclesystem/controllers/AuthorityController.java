package com.birkle.vehiclesystem.controllers;

import com.birkle.vehiclesystem.exceptions.AuthorityNotFoundException;
import com.birkle.vehiclesystem.models.Authority;
import com.birkle.vehiclesystem.services.security.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Controller to handle Authority requests
 *
 * @author VinodJohn
 */
@RestController
@RequestMapping("/authority")
public class AuthorityController {

    @Autowired
    private AuthorityService authorityService;

    @GetMapping("")
    private List<Authority> getAllAuthorities() {
        return authorityService.getAllAuthorities();
    }

    @GetMapping("/{id}")
    private Authority getAuthority(@PathVariable("id") UUID id) throws AuthorityNotFoundException {
        return authorityService.getAuthorityById(id);
    }

    @PostMapping("/create")
    private ResponseEntity<Authority> createAuthority(@Valid @RequestBody Authority authority) {
        authorityService.createAuthority(authority);
        return new ResponseEntity<>(authority, HttpStatus.CREATED);
    }

    @PostMapping("/update/{id}")
    private ResponseEntity<Authority> updateAuthority(@Valid @RequestBody Authority authority, @PathVariable("id") UUID id) throws AuthorityNotFoundException {
        authority.setId(id);
        authorityService.updateAuthority(authority);
        return new ResponseEntity<>(authority, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    private ResponseEntity<?> deleteAuthority(@PathVariable("id") UUID id) throws AuthorityNotFoundException {
        authorityService.deleteAuthorityById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    private ResponseEntity<?> restoreAuthority(@PathVariable("id") UUID id) throws AuthorityNotFoundException {
        authorityService.restoreAuthorityById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
