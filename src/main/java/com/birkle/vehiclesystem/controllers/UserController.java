package com.birkle.vehiclesystem.controllers;

import com.birkle.vehiclesystem.exceptions.ExternalSessionExpiredException;
import com.birkle.vehiclesystem.exceptions.ExternalSessionNotFoundException;
import com.birkle.vehiclesystem.exceptions.UserNotFoundException;
import com.birkle.vehiclesystem.models.User;
import com.birkle.vehiclesystem.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

/**
 * Controller to handle User requests
 *
 * @author VinodJohn
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    private List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    private User getUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        return userService.getUserById(id);
    }

    @PostMapping("/create")
    private ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PostMapping("/update/{id}")
    private ResponseEntity<User> updateUser(@Valid @RequestBody User user, @PathVariable("id") UUID id) throws UserNotFoundException {
        user.setId(id);
        userService.updateUser(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    private ResponseEntity<?> deleteUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        userService.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    private ResponseEntity<?> restoreUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        userService.restoreUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/activate/{sessionId}")
    private ResponseEntity<?> activateUser(@PathVariable("sessionId") UUID sessionId) throws
            ExternalSessionNotFoundException, ExternalSessionExpiredException {
        userService.activateUserById(sessionId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/reset-password-generate/{id}")
    private ResponseEntity<?> resetPasswordGenerate(@PathVariable("id") String email) throws UserNotFoundException {
        userService.resetUserPasswordByEmail(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/reset-password/{sessionId}")
    private ResponseEntity<?> resetPassword(@PathVariable("sessionId") UUID sessionId,
                                            @Valid @Size(min = 6, max = 25, message = "{messages.constraints.password-length}")
                                            @RequestBody String password)
            throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        userService.resetUserPassword(sessionId, password);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
