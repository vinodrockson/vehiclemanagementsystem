package com.birkle.vehiclesystem.controllers;

import com.birkle.vehiclesystem.handlers.exception.models.ErrorResponse;
import com.birkle.vehiclesystem.models.User;
import com.birkle.vehiclesystem.models.UserClientResponse;
import com.birkle.vehiclesystem.services.security.AuthenticationService;
import com.birkle.vehiclesystem.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller to handle authentication requests
 *
 * @author VinodJohn
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public ResponseEntity<?> login() {
        User user = userService.getUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        UserClientResponse userClientResponse = new UserClientResponse(user.getEmail(), user.isEmailActivated(), user.isActive(),
                user.getAuthority().getName());

        if (!user.isEmailActivated()) {
            return handleUserEmailNotActivated();
        } else if (!user.isActive()) {
            return handleUserAccountInactive();
        }

        return new ResponseEntity<>(userClientResponse, HttpStatus.OK);
    }

    @GetMapping("/session-invalid")
    public void handleInvalidSession() {
        authenticationService.notifyInvalidSessionToClient();
    }

    // PRIVATE METHODS //
    private ResponseEntity<ErrorResponse> handleUserEmailNotActivated() {
        List<String> details = new ArrayList<>();
        details.add("Account not activated yet! Please do activate via link sent to your email.");
        ErrorResponse error = new ErrorResponse("Access denied!", details);
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<ErrorResponse> handleUserAccountInactive() {
        List<String> details = new ArrayList<>();
        details.add("Your account has been disabled! Please contact administrator.");
        ErrorResponse error = new ErrorResponse("Access denied!", details);
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }
}
