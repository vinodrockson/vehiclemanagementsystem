package com.birkle.vehiclesystem.controllers;

import com.birkle.vehiclesystem.exceptions.VehicleNotFoundException;
import com.birkle.vehiclesystem.models.Vehicle;
import com.birkle.vehiclesystem.services.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Controller to handle Vehicle requests
 *
 * @author VinodJohn
 */
@RestController
@RequestMapping("/vehicle")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("")
    private List<Vehicle> getAllVehicles() {
        return vehicleService.getAllVehicles();
    }

    @GetMapping("/{id}")
    private Vehicle getVehicle(@PathVariable("id") UUID id) throws VehicleNotFoundException {
        return vehicleService.getVehicleById(id);
    }

    @PostMapping("/create")
    private ResponseEntity<Vehicle> createVehicle(@Valid @RequestBody Vehicle vehicle) {
        vehicleService.createVehicle(vehicle);
        return new ResponseEntity<>(vehicle, HttpStatus.CREATED);
    }

    @PostMapping("/update/{id}")
    private ResponseEntity<Vehicle> updateVehicle(@Valid @RequestBody Vehicle vehicle, @PathVariable("id") UUID id) throws VehicleNotFoundException {
        vehicle.setId(id);
        vehicleService.updateVehicle(vehicle);
        return new ResponseEntity<>(vehicle, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    private ResponseEntity<?> deleteVehicle(@PathVariable("id") UUID id) throws VehicleNotFoundException {
        vehicleService.deleteVehicleById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    private ResponseEntity<?> restoreVehicle(@PathVariable("id") UUID id) throws VehicleNotFoundException {
        vehicleService.restoreVehicleById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
