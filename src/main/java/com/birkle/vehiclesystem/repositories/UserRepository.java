package com.birkle.vehiclesystem.repositories;

import com.birkle.vehiclesystem.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository to handle User related data queries
 *
 * @author VinodJohn
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByEmail(String email);
}
