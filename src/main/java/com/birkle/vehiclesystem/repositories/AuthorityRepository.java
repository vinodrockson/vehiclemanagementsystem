package com.birkle.vehiclesystem.repositories;

import com.birkle.vehiclesystem.models.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository to handle Authority related data queries
 *
 * @author VinodJohn
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, UUID> {
    Authority findByName(String name);
}
