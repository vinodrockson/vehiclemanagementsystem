package com.birkle.vehiclesystem.repositories;

import com.birkle.vehiclesystem.models.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository to handle Vehicle related data queries
 *
 * @author VinodJohn
 */
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, UUID> {
    Vehicle findByVin(String vin);
}
