package com.birkle.vehiclesystem.repositories;

import com.birkle.vehiclesystem.models.ExternalSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository to handle ExternalSession data queries
 *
 * @author VinodJohn
 */
@Repository
public interface ExternalSessionRepository extends JpaRepository<ExternalSession, UUID> {
}
