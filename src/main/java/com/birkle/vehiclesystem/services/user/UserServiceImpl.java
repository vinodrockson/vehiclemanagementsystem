package com.birkle.vehiclesystem.services.user;

import com.birkle.vehiclesystem.exceptions.ExternalSessionExpiredException;
import com.birkle.vehiclesystem.exceptions.ExternalSessionNotFoundException;
import com.birkle.vehiclesystem.exceptions.UserNotFoundException;
import com.birkle.vehiclesystem.models.Email;
import com.birkle.vehiclesystem.models.ExternalSession;
import com.birkle.vehiclesystem.models.ExternalSessionType;
import com.birkle.vehiclesystem.models.User;
import com.birkle.vehiclesystem.repositories.UserRepository;
import com.birkle.vehiclesystem.services.mail.ExternalSessionService;
import com.birkle.vehiclesystem.services.mail.MailService;
import com.birkle.vehiclesystem.services.security.AuthorityService;
import com.birkle.vehiclesystem.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.*;

import static com.birkle.vehiclesystem.utils.Constants.Security.AUTHORITY_REGULAR_USER;

/**
 * Implementation of User Service
 *
 * @author VinodJohn
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ExternalSessionService externalSessionService;

    @Autowired
    private MailService mailService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (user.getAuthority() == null) {
            user.setAuthority(authorityService.getAuthorityByName(AUTHORITY_REGULAR_USER));
        }

        user = userRepository.saveAndFlush(user);

        try {
            mailService.sendEmail(getActivationEmail(user));
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void updateUser(User user) throws UserNotFoundException {
        if (!userRepository.existsById(user.getId())) {
            throw new UserNotFoundException(Objects.requireNonNull(user).getId());
        }

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        if (!user.getPassword().equals(encodedPassword)) {
            user.setPassword(encodedPassword);
        }

        if (user.getAuthority() == null) {
            user.setAuthority(authorityService.getAuthorityByName(AUTHORITY_REGULAR_USER));
        }

        userRepository.saveAndFlush(user);
    }

    @Override
    public User getUserById(UUID id) throws UserNotFoundException {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUserById(UUID id) throws UserNotFoundException {
        User user = getUserById(id);
        user.setActive(false);
        userRepository.saveAndFlush(user);

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Account deleted - Vehicle Management System");
        email.setMessage("<h1>Goodbye!</h1><p>Thank you for using our service!</p>");

        try {
            mailService.sendEmail(email);
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void restoreUserById(UUID id) throws UserNotFoundException {
        User user = getUserById(id);
        user.setActive(true);
        userRepository.saveAndFlush(user);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to login", Constants.Client.BASE_URL);

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Account recovered - Vehicle Management System");
        email.setMessage("<h1>Hooray!</h1><p>Your account has been restored!</p>");
        email.setLinks(links);

        try {
            mailService.sendEmail(email);
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void activateUserById(UUID sessionId) throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        ExternalSession externalSession = externalSessionService.getSessionById(sessionId);

        if (LocalDateTime.now().isAfter(externalSession.getExpiryDate())) {
            externalSessionService.deleteSession(externalSession);

            try {
                mailService.sendEmail(getActivationEmail(externalSession.getUser()));
            } catch (MessagingException e) {
                throw new RuntimeException("{messages.client.email-error}");
            }

            throw new ExternalSessionExpiredException();
        }

        User user = externalSession.getUser();
        user.setEmailActivated(true);
        user.setActive(true);
        userRepository.saveAndFlush(user);
        externalSessionService.deleteSession(externalSession);
    }

    @Override
    public void resetUserPasswordByEmail(String userEmail) throws UserNotFoundException {
        User user = getUserByEmail(userEmail);

        if (user == null) {
            throw new UserNotFoundException(null);
        }

        try {
            mailService.sendEmail(getResetPasswordEmail(user));
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void resetUserPassword(UUID sessionId, String password) throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        ExternalSession externalSession = externalSessionService.getSessionById(sessionId);

        if (LocalDateTime.now().isAfter(externalSession.getExpiryDate())) {
            externalSessionService.deleteSession(externalSession);

            try {
                mailService.sendEmail(getResetPasswordEmail(externalSession.getUser()));
            } catch (MessagingException e) {
                throw new RuntimeException("{messages.client.email-error}");
            }

            throw new ExternalSessionExpiredException();
        }

        User user = externalSession.getUser();
        user.setPassword(passwordEncoder.encode(password));
        userRepository.saveAndFlush(user);
        externalSessionService.deleteSession(externalSession);
    }

    // PRIVATE METHODS //
    private Email getActivationEmail(User user) {
        ExternalSession externalSession = new ExternalSession();
        externalSession.setUser(user);
        externalSession.setExternalSessionType(ExternalSessionType.CREATE_USER);
        externalSession.setUrl(Constants.Client.BASE_URL.concat(Constants.Client.USER_ACTIVATION_URL));
        externalSession.setExpiryDate(LocalDateTime.now().plusHours(Constants.Client.USER_ACTIVATION_URL_LIFE_HOURS));
        externalSession.setExpired(false);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to activate your account", externalSessionService.createNewSession(externalSession));

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Activation email - Vehicle Management System");
        email.setMessage("<h1>Welcome to Vehicle Management System</h1><p>Thank you for registering with us." +
                "The below link will be active for next " + Constants.Client.USER_ACTIVATION_URL_LIFE_HOURS + " hours.</p>");
        email.setLinks(links);

        return email;
    }

    private Email getResetPasswordEmail(User user) {
        ExternalSession externalSession = new ExternalSession();
        externalSession.setUser(user);
        externalSession.setExternalSessionType(ExternalSessionType.RESET_USER_PASSWORD);
        externalSession.setUrl(Constants.Client.BASE_URL.concat(Constants.Client.RESET_PASSWORD_URL));
        externalSession.setExpiryDate(LocalDateTime.now().plusMinutes(Constants.Client.RESET_PASSWORD_LIFE_MINUTES));
        externalSession.setExpired(false);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to reset password for your account", externalSessionService.createNewSession(externalSession));

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Password reset - Vehicle Management System");
        email.setMessage("<h1>Reset password</h1><p>The below link will be active for next " +
                Constants.Client.RESET_PASSWORD_LIFE_MINUTES + " minutes.</p>");
        email.setLinks(links);

        return email;
    }
}
