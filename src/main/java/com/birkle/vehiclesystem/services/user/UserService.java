package com.birkle.vehiclesystem.services.user;


import com.birkle.vehiclesystem.exceptions.ExternalSessionExpiredException;
import com.birkle.vehiclesystem.exceptions.ExternalSessionNotFoundException;
import com.birkle.vehiclesystem.exceptions.UserNotFoundException;
import com.birkle.vehiclesystem.models.User;

import java.util.List;
import java.util.UUID;

/**
 * Service to handle User business logic and operations
 *
 * @author VinodJohn
 */
public interface UserService {

    /**
     * To create a new User
     *
     * @param user User
     */
    void createUser(User user);

    /**
     * To update an existing User
     *
     * @param user user
     */
    void updateUser(User user) throws UserNotFoundException;

    /**
     * To get user by ID
     *
     * @param id User ID
     * @return User
     */
    User getUserById(UUID id) throws UserNotFoundException;

    /**
     * To get user by email
     *
     * @param email User email
     * @return User
     */
    User getUserByEmail(String email);

    /**
     * To get all the users
     *
     * @return list of all users
     */
    List<User> getAllUsers();

    /**
     * Delete user(change active status) by ID
     *
     * @param id User ID
     */
    void deleteUserById(UUID id) throws UserNotFoundException;

    /**
     * Restore user(change active state) by ID
     *
     * @param id User ID
     */
    void restoreUserById(UUID id) throws UserNotFoundException;

    /**
     * Activate user by ID
     *
     * @param sessionId User ID
     */
    void activateUserById(UUID sessionId) throws ExternalSessionNotFoundException, ExternalSessionExpiredException;

    /**
     * Reset password for existing user by email
     *
     * @param userEmail User email
     */
    void resetUserPasswordByEmail(String userEmail) throws UserNotFoundException;

    /**
     * Reset password for existing user
     *
     * @param sessionId SessionId to reset password
     * @param password new password for user
     */
    void resetUserPassword(UUID sessionId, String password) throws ExternalSessionNotFoundException, ExternalSessionExpiredException;
}

