package com.birkle.vehiclesystem.services.vehicle;

import com.birkle.vehiclesystem.exceptions.VehicleNotFoundException;
import com.birkle.vehiclesystem.models.Vehicle;
import com.birkle.vehiclesystem.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation of Vehicle Service
 *
 * @author VinodJohn
 */
@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public void createVehicle(Vehicle vehicle) {
        vehicle.setActive(true);
        vehicleRepository.saveAndFlush(vehicle);
    }

    @Override
    public void updateVehicle(Vehicle vehicle) throws VehicleNotFoundException {
        if (!vehicleRepository.existsById(vehicle.getId())) {
            throw new VehicleNotFoundException(Objects.requireNonNull(vehicle).getId());
        }
        vehicleRepository.saveAndFlush(vehicle);
    }

    @Override
    public Vehicle getVehicleById(UUID id) throws VehicleNotFoundException {
        return vehicleRepository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException(id));
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return vehicleRepository.findAll();
    }

    @Override
    public Vehicle getVehicleByVIN(String vin) {
        return vehicleRepository.findByVin(vin);
    }

    @Override
    public void deleteVehicleById(UUID id) throws VehicleNotFoundException {
        Vehicle vehicle = getVehicleById(id);
        vehicle.setActive(false);
        vehicleRepository.saveAndFlush(vehicle);
    }

    @Override
    public void restoreVehicleById(UUID id) throws VehicleNotFoundException {
        Vehicle vehicle = getVehicleById(id);
        vehicle.setActive(true);
        vehicleRepository.saveAndFlush(vehicle);
    }
}
