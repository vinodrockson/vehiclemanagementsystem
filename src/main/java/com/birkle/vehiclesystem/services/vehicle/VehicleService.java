package com.birkle.vehiclesystem.services.vehicle;

import com.birkle.vehiclesystem.exceptions.VehicleNotFoundException;
import com.birkle.vehiclesystem.models.Vehicle;

import java.util.List;
import java.util.UUID;

/**
 * Service to handle Vehicle business logic and operations
 *
 * @author VinodJohn
 */
public interface VehicleService {

    /**
     * To create a new Vehicle
     *
     * @param vehicle Vehicle
     */
    void createVehicle(Vehicle vehicle);

    /**
     * To update an existing Vehicle
     *
     * @param vehicle vehicle
     */
    void updateVehicle(Vehicle vehicle) throws VehicleNotFoundException;

    /**
     * To get vehicle by ID
     *
     * @param id Vehicle ID
     * @return Vehicle
     */
    Vehicle getVehicleById(UUID id) throws VehicleNotFoundException;

    /**
     * To get all the vehicles
     *
     * @return list of all vehicles
     */
    List<Vehicle> getAllVehicles();

    /**
     * To get vehicle by VIN
     *
     * @param vin VIN of vehicle
     * @return Vehicle
     */
    Vehicle getVehicleByVIN(String vin);

    /**
     * Delete vehicle(change active status) by ID
     *
     * @param id Vehicle ID
     */
    void deleteVehicleById(UUID id) throws VehicleNotFoundException;

    /**
     * Restore vehicle(change active state) by ID
     *
     * @param id Vehicle id
     */
    void restoreVehicleById(UUID id) throws VehicleNotFoundException;
}
