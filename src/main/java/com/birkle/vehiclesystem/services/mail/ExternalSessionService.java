package com.birkle.vehiclesystem.services.mail;

import com.birkle.vehiclesystem.exceptions.ExternalSessionNotFoundException;
import com.birkle.vehiclesystem.exceptions.UserNotFoundException;
import com.birkle.vehiclesystem.models.ExternalSession;

import java.util.UUID;

/**
 * Service to handle ExternalSession related operations
 *
 * @author VinodJohn
 */
public interface ExternalSessionService {

    /**
     * To create new external session
     *
     * @param externalSession ExternalSession
     * @return URL of session
     */
    String createNewSession(ExternalSession externalSession);

    /**
     * To get a session by ID
     *
     * @param id ExternalSession ID
     * @return ExternalSession
     */
    ExternalSession getSessionById(UUID id) throws ExternalSessionNotFoundException;


    /**
     * To delete existing external session
     *
     * @param externalSession ExternalSession
     */
    void deleteSession(ExternalSession externalSession);
}
