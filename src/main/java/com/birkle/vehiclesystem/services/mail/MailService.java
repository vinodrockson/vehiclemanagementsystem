package com.birkle.vehiclesystem.services.mail;

import com.birkle.vehiclesystem.models.Email;

import javax.mail.MessagingException;

/**
 * Service is to send emails

 * @author VinodJohn
 */
public interface MailService {
    /**
     * To send an email
     *
     * @param email Email
     */
    void sendEmail(Email email) throws MessagingException;
}
