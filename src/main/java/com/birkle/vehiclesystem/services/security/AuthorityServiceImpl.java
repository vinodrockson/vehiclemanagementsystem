package com.birkle.vehiclesystem.services.security;

import com.birkle.vehiclesystem.exceptions.AuthorityNotFoundException;
import com.birkle.vehiclesystem.exceptions.UserNotFoundException;
import com.birkle.vehiclesystem.models.Authority;
import com.birkle.vehiclesystem.repositories.AuthorityRepository;
import com.birkle.vehiclesystem.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation of Authority Service
 *
 * @author VinodJohn
 */
@Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {
    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserService userService;

    @Override
    public void createAuthority(Authority authority) {
        authority.setName(authority.getName().toUpperCase().replace(" ", "_"));
        authority.setActive(true);
        authorityRepository.saveAndFlush(authority);
    }

    @Override
    public void updateAuthority(Authority authority) throws AuthorityNotFoundException {
        if (!authorityRepository.existsById(authority.getId())) {
            throw new AuthorityNotFoundException(Objects.requireNonNull(authority).getId());
        }

        authority.setName(authority.getName().toUpperCase().replace(" ", "_"));
        authorityRepository.saveAndFlush(authority);
    }

    @Override
    public Authority getAuthorityById(UUID id) throws AuthorityNotFoundException {
        return authorityRepository.findById(id)
                .orElseThrow(() -> new AuthorityNotFoundException(id));
    }

    @Override
    public Authority getAuthorityByName(String name) {
        return authorityRepository.findByName(name);
    }

    @Override
    public List<Authority> getAllAuthorities() {
        return authorityRepository.findAll();
    }

    @Override
    public void deleteAuthorityById(UUID id) throws AuthorityNotFoundException {
        Authority authority = getAuthorityById(id);
        authority.setActive(false);
        authorityRepository.saveAndFlush(authority);

        // When a role is deleted, all user with this role will switch to default user.
        userService.getAllUsers().forEach(user -> {
            if (user.getAuthority().getId().equals(id)) {
                user.setAuthority(null);
                try {
                    userService.updateUser(user);
                } catch (UserNotFoundException e) {
                    throw new RuntimeException("Error in updating users");
                }
            }
        });
    }

    @Override
    public void restoreAuthorityById(UUID id) throws AuthorityNotFoundException {
        Authority authority = getAuthorityById(id);
        authority.setActive(true);
        authorityRepository.saveAndFlush(authority);
    }
}
