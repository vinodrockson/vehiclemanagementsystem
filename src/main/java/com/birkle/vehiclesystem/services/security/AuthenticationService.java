package com.birkle.vehiclesystem.services.security;

/**
 * Service to handle authentication related operations
 *
 * @author VinodJohn
 */
public interface AuthenticationService {
    /**
     * To notify session invalid to registered client
     */
    void notifyInvalidSessionToClient();
}
