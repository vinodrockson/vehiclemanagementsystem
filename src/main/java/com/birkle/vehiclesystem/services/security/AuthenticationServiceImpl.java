package com.birkle.vehiclesystem.services.security;

import com.birkle.vehiclesystem.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service implementation of AuthenticationService
 *
 * @author VinodJohn
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void notifyInvalidSessionToClient() {
        restTemplate.postForEntity(Constants.Client.BASE_URL.concat(Constants.Client.SESSION_EXPIRED_URL),
                null, Void.class);
    }
}
