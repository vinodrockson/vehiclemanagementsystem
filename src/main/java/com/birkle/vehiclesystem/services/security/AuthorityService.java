package com.birkle.vehiclesystem.services.security;

import com.birkle.vehiclesystem.exceptions.AuthorityNotFoundException;
import com.birkle.vehiclesystem.models.Authority;

import java.util.List;
import java.util.UUID;

/**
 * Service to handle Authority business logic and operations
 *
 * @author VinodJohn
 */
public interface AuthorityService {

    /**
     * To create a new Authority
     *
     * @param authority Authority
     */
    void createAuthority(Authority authority);

    /**
     * To update an existing Authority
     *
     * @param authority authority
     */
    void updateAuthority(Authority authority) throws AuthorityNotFoundException;

    /**
     * To get authority by ID
     *
     * @param id Authority ID
     * @return Authority
     */
    Authority getAuthorityById(UUID id) throws AuthorityNotFoundException;

    /**
     * To get user by name
     *
     * @param name Authority name
     * @return Authority
     */
    Authority getAuthorityByName(String name);

    /**
     * To get all the authorities
     *
     * @return list of all authorities
     */
    List<Authority> getAllAuthorities();

    /**
     * Delete authority(change active status) by ID
     *
     * @param id Authority ID
     */
    void deleteAuthorityById(UUID id) throws AuthorityNotFoundException;

    /**
     * Restore authority(change active state) by ID
     *
     * @param id Authority id
     */
    void restoreAuthorityById(UUID id) throws AuthorityNotFoundException;
}
