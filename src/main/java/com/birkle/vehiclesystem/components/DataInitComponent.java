package com.birkle.vehiclesystem.components;

import com.birkle.vehiclesystem.models.Authority;
import com.birkle.vehiclesystem.models.User;
import com.birkle.vehiclesystem.services.security.AuthorityService;
import com.birkle.vehiclesystem.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.birkle.vehiclesystem.utils.Constants.Security.*;

/**
 * Component to initialize data on startup
 *
 * @author VinodJohn
 */
@Component
public class DataInitComponent {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorityService authorityService;

    @PostConstruct
    private void initUserAndAuthorityData() {
        if (authorityService.getAuthorityByName(AUTHORITY_ADMIN) == null) {
            Authority authority = new Authority();
            authority.setName(AUTHORITY_ADMIN);
            authorityService.createAuthority(authority);
        }

        if (authorityService.getAuthorityByName(AUTHORITY_REGULAR_USER) == null) {
            Authority authority = new Authority();
            authority.setName(AUTHORITY_REGULAR_USER);
            authorityService.createAuthority(authority);
        }

        if (authorityService.getAuthorityByName(AUTHORITY_SUPER_USER) == null) {
            Authority authority = new Authority();
            authority.setName(AUTHORITY_SUPER_USER);
            authorityService.createAuthority(authority);
        }

        if (userService.getUserByEmail(DEFAULT_USER_ADMIN_EMAIL) == null) {
            Authority adminAuthority = authorityService.getAuthorityByName(AUTHORITY_ADMIN);

            User user = new User();
            user.setEmail(DEFAULT_USER_ADMIN_EMAIL);
            user.setPassword(DEFAULT_USER_ADMIN_PASSWORD);
            user.setActive(true);
            user.setEmailActivated(true);
            user.setAuthority(adminAuthority);
            userService.createUser(user);
        }
    }
}
